/**
 * 服务器域名获取
 */
const url = require('configUrl.js');
const apiUrl = url.apiUrl

module.exports = {
  apiUrl: apiUrl + '/'
};