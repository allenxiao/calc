const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

var toTimestamp = date => {
  var nDate = new Date(date.replace(/-/g, '/'));
  var timestamp = Date.parse(nDate);
  return timestamp / 1000
}

//中国标准时间转换时间格式
const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()
  return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

//中国标准时间转换日期格式
const formatDate = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  return [year, month, day].map(formatNumber).join('-')
}

// 关键字提取
const getKeywords = (keywords, searchKeywords) => {
  // 用来显示的已选条件的数据
  var keywords = keywords || []
  //获取搜索框的搜索条件
  var searchKeywords = searchKeywords.trim().split(/\s+/);
  // 去除空格
  for (var i = 0; i < searchKeywords.length; i++) {
    if (searchKeywords[i] == '' || searchKeywords[i] == null || typeof (searchKeywords[i]) == undefined) {
      searchKeywords.splice(i, 1);
      i = i - 1;
    }
  }
  // 对keywords 进行增加
  keywords = keywords.concat(searchKeywords);
  // 数组去重
  var hash = [];
  for (var i = 0; i < keywords.length; i++) {
    for (var j = i + 1; j < keywords.length; j++) {
      if (keywords[i] === keywords[j]) {
        ++i;
      }
    }
    hash.push(keywords[i]);
  }
  return hash
}

module.exports = {
  formatTime: formatTime,
  formatDate: formatDate,
  toTimestamp: toTimestamp,
  getKeywords: getKeywords
}