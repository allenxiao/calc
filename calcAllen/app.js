//app.js
App({
  token: wx.getStorageSync('token'), //登录返回的个人信息

  globalData: {
    systemInfo: null,
    windowHeight: null, // rpx换算px后的窗口高度
    screenHeight: null, // rpx换算px后的屏幕高度
    statusBarHeight: null, // 导航头的高度
    userInfo: null,
    // appid: 'wx1caf90dce0e26d7d',
    // secret: 'f0f42c26186bfc1d3d454e441492e037'
  },

  getOpenId: function () {
    let openId = '';
    let that = this;
    wx.login({
      success: function (res) {
        if (res.code) {
          var d = that.globalData; //这里存储了appid、secret、token串  
          var url = 'https://api.weixin.qq.com/sns/jscode2session?appid=' + d.appid + '&secret=' + d.secret + '&js_code=' + res.code + '&grant_type=authorization_code';
          wx.request({
            url: url,
            data: {},
            method: 'GET',
            success: function (res) {
              openId = res.data.openid;
              wx.setStorageSync('openId', openId); //存储openid  
            }
          });
        } else {
          console.log('获取用户openId失败！' + res.errMsg)
        }
      }
    })
    return openId;
  },

  onLaunch: function () {
    if (!wx.getStorageSync('openId')) {
      this.getOpenId();
    }
    wx.onAppRoute(res => { // 路由,页面的每次跳转都执行了
      let pages = getCurrentPages();
      let view = pages[pages.length - 1];
      wx.showShareMenu({
        withShareTicket: true
      });
      if (view.route === "pages/calc/buyCar") {
        // 详情页的转发(用户进入时候,应该再次发送请求进行数据的获得)
        return {
          title: '乐意计算器',
          path: "pages/calc/buyCar"
        }
      }
      // 分享的处理
      view.onShareAppMessage = function (e) {
        return {
          title: "乐意计算器",
          path: "pages/calc/buyCar"
        }
      }
    })
    // 获取系统信息
    wx.getSystemInfo({
      success: function (res) {
        var model = res.system;
        var platform = res.platform;
        wx.setStorageSync('appModel', model)
        wx.setStorageSync('platform', platform)
      },
    })
    this.loadFirstEnter()
  },

  //第一次登录加载的函数
  loadFirstEnter: function (more_scene) {
    this.token = wx.getStorageSync('token');
  },

  //检查是否已经登录
//   checkUserLogin: function () {
//     this.token = wx.getStorageSync('token');
//     if (!this.token) {
//       this.showModalInfo('用户未登录', false, '../login/login');
//     }
//     return this.token;
//   },

  //仅显示错误
  packedCheckResponse: function (res, isShowError = true) {
    var isSuccess = res.code == 200;
    if (!isSuccess && isShowError) {
      this.showErrorMessage(res.msg);
    }
    return isSuccess;
  },

  //成功/失败均显示
  packedSubmitResponse: function (res, isShow = true) {
    var isSuccess = res.code == 200;
    if (isShow) {
      this.showMessage(isSuccess, res.msg);
    }
    return isSuccess;
  },

  //非空判断
  verifyRequire: function (value, name) {
    if (!value) {
      this.showErrorMessage(name + '不能为空！');
    }
  },

  backOnload: function () {
    var pages = getCurrentPages();
    var prevPage = pages[pages.length - 2];
    wx.navigateBack({
      delta: 1,
      success: function () {
        prevPage.onLoad()
      }
    })
  },

  showToastLoading: function (title = '加载中', mask = true) {
    wx.showLoading({
      title: title,
      mask: mask
    })
  },

  showErrorMessage: function (title, time = 2000) {
    this.showMessage(0, title, time);
  },

  showSuccessMessage: function (title, time = 2000) {
    this.showMessage(1, title, time);
  },

  showMessage: function (isSuccess, title, time = 2000) {
    if (title.length > 7) {
      wx.showToast({
        title: title,
        icon: 'none',
        duration: time
      });
    } else {
      wx.showToast({
        title: title,
        image: isSuccess ? '/images/icons/success.png' : '/images/icons/error.png',
        duration: time
      })
    }
  }
})