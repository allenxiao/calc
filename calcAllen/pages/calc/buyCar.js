const app = getApp();
// 验证插件
import WxValidate from '../../utils/WxValidate.js'

Page({
    /**
     * 页面的初始数据
     */
    data: {
        disableInput: 0,
        btnFlag: 0,
        guidancePrice: '', // 指导价格
        preferentialPrice: '', // 优惠价格
        baseCarPrice: '', // 裸车价格
        purchaseTax: '', // 购置税
        insurePrice: '', // 保险预估
        otherPrice: '', // 其他费用
        startOffPrice: '', // 上路价格
        saveBtn: true,
        baseRateArrays: ['12', '13', '14', '15', '16', '17', '18'],
    },

    computeSubmit: function (e) {
        const params = e.detail.value
        //校验表单，验证表单信息
        if (!this.WxValidate.checkForm(params)) {
            const error = this.WxValidate.errorList[0]
            app.showErrorMessage(error.msg);
            return;
        }
        var that = this;
        var baseCarPrice = that.data.baseCarPrice;
        var purchaseTax = that.data.purchaseTax;
        var insurePrice = that.data.insurePrice;
        var otherPrice = that.data.otherPrice;
        if (!otherPrice) {
            otherPrice = 0;
        }
        if (!purchaseTax) {
            purchaseTax = 0;
        }
        that.setData({
            startOffPrice: (parseFloat(baseCarPrice) + parseFloat(purchaseTax) + parseFloat(insurePrice) + parseFloat(otherPrice)).toFixed(2),
            btnFlag: 1,
        });
    },
    bindGuidancePrice: function (e) {
        var that = this;
        // 指导价格
        var guidancePrice = e.detail.value;
        // if(this.checkInputSpecial(guidancePrice, 'guidancePrice')){
        //     return;
        // };
        if(guidancePrice != that.data.guidancePrice){
            that.setData({
                btnFlag: 0
            });
        }
        if (that.data.preferentialPrice) {
            that.data.baseCarPrice = guidancePrice - that.data.preferentialPrice;
        }
        var purchaseTax = (guidancePrice / 11.3).toFixed(2);
        that.setData({
            guidancePrice: guidancePrice,
            purchaseTax: purchaseTax,
        });
        this.checkInput();
        
        // this.checkInputTrue();
    },
    // 优惠点数
    bindPreferentialDot: function(e){
        var that = this;
        // 优惠点数
        var preferentialDot = e.detail.value;
        if(preferentialDot != that.data.preferentialDot){
            that.setData({
              btnFlag: 0
            })
        }
        var preferentialPrice = 0;
        if(that.data.guidancePrice){
            preferentialPrice = that.data.guidancePrice * (preferentialDot / 100);
            var baseCarPrice = that.data.guidancePrice - preferentialPrice;
            var purchaseTax = (baseCarPrice / 11.3).toFixed(2);
            that.setData({
                baseCarPrice: baseCarPrice.toFixed(2),
                purchaseTax: purchaseTax,
                preferentialPrice: preferentialPrice.toFixed(2),
                preferentialDot: preferentialDot
            });
        }

    },

    checkInputSpecial: function(params, key) {
        var flag = false;
        var that = this;
        if(params.toString().indexOf('.') != -1){
            var arry = params.split(".");
            if(arry.length > 2 || (arry[0] == "" && arry[1] == "")){
                params = "";
            }else{
                if(arry[1] == ""){
                    params = params+"00";
                }
             }
             that.setData({
                [key]: params
            });
            flag = true;
        }
        //校验表单，验证表单信息
        if (params && !(/^\d+(\.\d{1,2})?$/.test(params))) {
            that.setData({
                [key]: ""
            });
            flag = true;
        }
        return flag;
    },
    bindPreferentialPrice: function (e) {
        var that = this;
        // 优惠价格
        var preferentialPrice = e.detail.value;
        // if(this.checkInputSpecial(preferentialPrice, 'preferentialPrice')){
        //     return;
        // };
        if(preferentialPrice != that.data.preferentialPrice){
            that.setData({
                btnFlag: 0
            });
        }
        if (that.data.guidancePrice) {
            var baseCarPrice = that.data.guidancePrice - preferentialPrice;
            var purchaseTax = (baseCarPrice / 11.3).toFixed(2);
            var preferentialDot = (preferentialPrice / that.data.guidancePrice)*100;
            that.setData({
                baseCarPrice: baseCarPrice.toFixed(2),
                purchaseTax: purchaseTax,
                preferentialPrice: preferentialPrice,
                preferentialDot: preferentialDot.toFixed(2)
            });
        }
        this.checkInput();
        // this.checkInputTrue();
    },

    bindInsurePrice: function (e) {
        var that = this;
        var insurePrice = e.detail.value;
        // if(this.checkInputSpecial(insurePrice, 'insurePrice')){
        //     return;
        // };
        if(insurePrice != that.data.insurePrice){
            that.setData({
                btnFlag: 0
            });
        }
        that.setData({
            insurePrice: insurePrice
        });
        this.checkInput();
        // this.checkInputTrue();
    },
    bindOtherPrice: function (e) {
        var that = this;
        var otherPrice = e.detail.value;
        // if(this.checkInputSpecial(otherPrice, 'otherPrice')){
        //     return;
        // };
        if(otherPrice != that.data.otherPrice){
            that.setData({
                btnFlag: 0
            });
        }
        that.setData({
            otherPrice: otherPrice
        });
        this.checkInput();
        // this.checkInputTrue();
    },
    bindBaseCarPrice: function (e) {
        var that = this;
        var baseCarPrice = e.detail.value;
        // if(this.checkInputSpecial(baseCarPrice, 'baseCarPrice')){
        //     return;
        // };
        if(baseCarPrice != that.data.baseCarPrice){
            that.setData({
                btnFlag: 0
            });
        }
        that.setData({
            baseCarPrice: baseCarPrice
        });
        this.checkInput();
        // this.checkInputTrue();
    },
    bindPurchaseTax: function (e) {
        var that = this;
        var purchaseTax = e.detail.value;
        // if(this.checkInputSpecial(purchaseTax, 'purchaseTax')){
        //     return;
        // };
        if(purchaseTax != that.data.purchaseTax){
            that.setData({
                btnFlag: 0
            });
        }
        that.setData({
            purchaseTax: purchaseTax
        });
        // this.checkInput();
        // this.checkInputTrue();
    },
    checkInput: function () {
        var that = this;
        if (that.data.guidancePrice && that.data.insurePrice 
            && that.data.preferentialPrice && that.data.baseCarPrice) {
            that.setData({
                saveBtn: false
            });
        }
    },
    checkInputTrue: function () {
        var that = this;
        if (that.data.guidancePrice == "" || that.data.insurePrice == ""
            || that.data.preferentialPrice == "" || that.data.baseCarPrice == "") {
            that.setData({
                saveBtn: true,
                btnFlag: 0
            });
        }
    },
    btnClear: function () {
        var that = this;
        wx.showModal({
            title: '提示信息',
            content: '确定清除信息吗?',
            confirmColor: "#D66058",
            showCancel: true,
            success: function (res) {
              if (res.confirm) {
                that.setData({
                    guidancePrice: '', // 指导价格
                    preferentialPrice: '', // 优惠价格
                    baseCarPrice: '', // 裸车价格
                    purchaseTax: '', // 购置税
                    insurePrice: '', // 保险预估
                    otherPrice: '', // 其他费用
                    startOffPrice: '', // 上路价格
                    preferentialDot: '', // 优惠点数
                    btnFlag: 0,
                    saveBtn: true,
                })
              }
            }
          });
    },
    // 验证函数
    initValidate() {
        const rules = {
            guidancePrice: {
                required: true,
                number: true
            },
            preferentialPrice: {
                required: true,
                number: true
            },
            insurePrice: {
                required: true,
                number: true
            },
            otherPrice: {
                required: false,
                number: true
            },
        }
        const messages = {
            guidancePrice: {
                required: '请输入指导价格',
                number: '指导价格输入错误'
            },
            preferentialPrice: {
                required: '请输入优惠价格',
                number: '优惠价格输入错误'
            },
            insurePrice: {
                required: '请输入保险预估费用',
                number: '保险预估费用输入错误'
            },
            otherPrice: {
                required: '请输入其他费用',
                number: '其他费用输入错误'
            },
        }
        this.WxValidate = new WxValidate(rules, messages)
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (e) {
        this.initValidate() //验证规则函数
    },
    onReady: function (e) {

    },
    onShow: function () {}

})