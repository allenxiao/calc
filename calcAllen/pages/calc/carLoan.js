const app = getApp();
// 验证插件
import WxValidate from '../../utils/WxValidate.js'
Page({
    /**
     * 页面的初始数据
     */
    data: {
        loanAmount: '',
        repaymentType: '',
        baseRate: '',
        interest: '',
        term: '',
        saveBtn: true,
        repaymentTypeArrays: ['等额本息', '等额本金', '先息后本'],
        termArrays: ['1年(12个月)', '2年(24个月)', '3年(36个月)', '4年(48个月)', '5年(60个月)'],
    },

    computeSubmit: function (e) {
        var that = this;
        var loanAmount = that.data.loanAmount;
        var baseRate = that.data.baseRate;
        var term = that.data.term;

        // 200000*1.24=248000/60=4133.33
        if (term === '0') {
            term = 12;
        } else if (term === '1') {
            term = 24;
        } else if (term === '2') {
            term = 36;
        } else if (term === '3') {
            term = 48;
        } else if (term === '4') {
            term = 60;
        }
        const params = e.detail.value
        //校验表单，验证表单信息
        if (!this.WxValidate.checkForm(params)) {
            const error = this.WxValidate.errorList[0]
            app.showErrorMessage(error.msg);
            return;
        }
        // 利息
        var interest = loanAmount * (baseRate / 100);
        var residueMonthly = (parseFloat(interest) + parseFloat(loanAmount)) / term;

        that.setData({
            residueMonthly: residueMonthly.toFixed(2),
            interest: interest.toFixed(2),
            residueMonthly: residueMonthly.toFixed(2)
        });
    },
    // 基准利率选择
    bindBaseRateChange: function (e) {
        var that = this;
        var baseRate = e.detail.value;
        that.setData({
            baseRate: baseRate,
            interest: "",
            residueMonthly: ""
        });
        this.checkInput();
    },
    bindRepaymentTypeChange: function (e) {
        var that = this;
        that.setData({
            repaymentType: e.detail.value
        })
        this.checkInput();
    },
    bindTermChange: function (e) {
        var that = this;
        that.setData({
            term: e.detail.value,
            interest: "",
            residueMonthly: ""
        })
        this.checkInput();
    },
    bindLoanAmount: function (e) {
        var loanAmount = e.detail.value;
        // loanAmount.replace(/[^\d]/g,'')
        this.setData({
            loanAmount: loanAmount,
            interest: "",
            residueMonthly: ""
        });
        this.checkInput();
    },
    bindAllRate: function (e) {
        var allRate = e.detail.value;
        this.setData({
            allRate: allRate,
            interest: "",
            residueMonthly: ""
        });
        this.checkInput();
    },
    checkInput: function () {
        var that = this;
        if (that.data.loanAmount && that.data.baseRate && that.data.term) {
            this.setData({
                saveBtn: false
            });
        }
    },

    // 验证函数
    initValidate() {
        const rules = {
            loanAmount: {
                required: true,
                number: true
            },
            baseRate: {
                required: true,
                number: true
            }
        }
        const messages = {
            loanAmount: {
                required: '请输入贷款金额',
                number: '贷款金额输入错误'
            },
            baseRate: {
                required: '请输入费率',
                number: '费输入错误'
            }
        }
        this.WxValidate = new WxValidate(rules, messages)
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (e) {
        this.initValidate() //验证规则函数
    },
    onReady: function (e) {
        var that = this
        wx.getSystemInfo({
            success: function (res) {
                var platform = res.platform;
                that.setData({
                    platform: platform
                })
            },
        });
    },
    onShow: function () {}

})