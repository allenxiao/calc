Page({

    /**
     * 页面的初始数据
     */
    data: {
      idBack:"back",
      idAc:"clear",
      idn:"negative",
      idAdd:"+",
      idNine: "9",
      idEight: "8",
      idSeven: "7",
      idMinus: "-",
      idSix: "6",
      idFive: "5",
      idFour: "4",
      idMultiplication: "*",
      idThree: "3",
      idTwo: "2",
      idOne: "1",
      idDivide: "÷",
      idZero: "0",
      idDrop: ".",
      idHistory: "history",
      idEqual: "=",
      screenData:"0",
      lastIsOperator:false,
      arr:[],
      logs:[],



      // 尺寸
      windowHeight: 0,
      screenHeight: 0,
      statusBarHeight: 0,
      sty: 'dark', // 默认dark
      boxColor: '#000000',
      fontColor: '#ffffff',
      resSty: 'res-sty-dark',
      btnCtrlSty: 'btn-ctrl-sty-dark',
      btnNumSty: 'btn-num-sty-dark',
      btnHistorySty: 'btn-history-sty-dark',     // h
      btnEqualSty: 'btn-equal-sty-dark',     // =
      btnAddSty: 'btn-add-sty-dark',       // +
      btnSubtractSty: 'btn-subtract-sty-dark',  // -
      btnMultiplySty: 'btn-multiply-sty-dark',  // x
      btnDivideSty: 'btn-divide-sty-dark',    // ÷
      ac: 'AC',
      show: '0',
      showAll: '0',
      showFlag: 1,
      storageNumNum: 0,  // 存储的数
      operator: '',   // 运算符
      showLen: 0,
      onceNum: false,
      error: false
    },
  
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.changeSty();
    },
  
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {
      
    },
  
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
      
    },
  
    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {
      
    },
  
    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
      
    },
  
    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {
      
    },
  
    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
      
    },
  
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
      
    },
    changeSty: function () {
        if (this.data.sty == 'dark') {
          this.setData({
            boxColor: '#000000',
            resSty: 'res-sty-dark',
            btnCtrlSty: 'btn-ctrl-sty-dark',
            btnNumSty: 'btn-num-sty-dark',
            btnEqualSty: 'btn-equal-sty-dark',        // =
            btnAddSty: 'btn-add-sty-dark',            // +
            btnSubtractSty: 'btn-subtract-sty-dark',  // -
            btnMultiplySty: 'btn-multiply-sty-dark',  // x
            btnDivideSty: 'btn-divide-sty-dark'       // ÷
          });
        }
      },
    history:function(){
      wx.navigateTo({
        url: '../calc/history',
      })
    },
    btnClick:function(event){
      //==================打log看输出的数据
      console.log(event.target.id);
      //==================获得点击的数
      var id=event.target.id;
      if (!isNaN(id)) {
        if (this.data.onceNum != false) {
            this.data.show = '0';
            this.data.showLen = 0;
            this.data.onceNum = false;
            this.data.screenData = "0";
            this.data.arr = [];
          }
      }
      
      //如果点击的是退格键操作如下
      if(id==this.data.idBack){
        var data=this.data.screenData;
        if(data==0){
          //如果值为0，不用做退格操作
          return ;
        }
        //如果不等于0开始往后减少
        data=data.substring(0,data.length-1);
        if(data==""||data=="-"){
          //如果退格到只有""或者-的时候赋值为0
          data=0;
        }
        //将screenData的值赋值为新data
        this.setData({screenData:data});
        //退格要减掉
        this.data.arr.pop();
      }else if(id==this.data.idAc){
        //如果点击的是清屏键操作如下
        //设置当前的data=0
        this.setData({screenData:"0"});
        this.data.arr.length=0;
      }else if(id==this.data.idn){
        var data=this.data.screenData;
        if(data==0){
          return ;//如果等于0就直接返回
        }
        //获得第一个值判断是什么
        var firstWord=data.substring(0,1);
        if(firstWord=="-"){//得到的第一个值如果是-，则重新赋值
          data=data.substring(1,data.length);//如果是-，去掉第一个元素
          this.data.arr.shift();
        }else{
          data="-"+data;//如果原来是正数，则加上-之后返回回去
          this.data.arr.unshift("-");
        }
        this.setData({ screenData: data });
      }else if(id==this.data.idEqual){//点击=号需要得到相应的值
        
        var data=this.data.screenData;
        if(data==0){
          //如果输入的是0，就不需要处理
          return ;
        }
        //最后一个字符必须是数字点击=号才有意义，所以需要过滤一下
        var lastWord = data.substring(data.length-1,data.length);
        if (isNaN(lastWord)){
          return ;//说明不是数字，直接return
        }
        var num="";
        var lastOperator;
        var arr=this.data.arr;
        var optarr=[];
        for(var i in arr){
          if(isNaN(arr[i])==false||arr[i]==this.data.idDrop||arr[i]==this.data.idn){
            num+=arr[i];
          }else{
            lastOperator=arr[i];
            optarr.push(num);
            optarr.push(arr[i]);
            num="";
          }
        }
          optarr.push(Number(num));
          var result = Number(optarr[0]*1.0);
          console.log(result+'哈哈');
          for(var i=1;i<optarr.length;i++){
            if (isNaN(optarr[i])){
              //加减乘除的处理
              if(optarr[1]==this.data.idAdd){
                result+=Number(optarr[i+1]);
              }else if(optarr[1]==this.data.idAdd){
                result += Number(optarr[i+1]);
              }else if(optarr[1]==this.data.idMinus){
                result -= Number(optarr[i+1]);
              } else if (optarr[1] == this.data.idMultiplication){
                result *= Number(optarr[i+1]);
              } else if(optarr[1] == this.data.idDivide){
                result /= Number(optarr[i+1]);
              }
          }
        }
        //将每次计算结果保存到缓存中去
        this.data.logs.push(data+"="+result);
          wx.setStorageSync("callogs", this.data.logs);
          //取值
          console.log(wx.getStorageSync('callogs'));
        //计算完成后清空数组
        this.data.arr.length=0;
        this.setData({screenData:result+""});
        // result = "";
        this.data.arr.push(result);
        this.data.onceNum = true;
      }else{
        //==================将数据叠加在后面之前，先判断lastIsOperator是否是true，且不是0
        if (id == this.data.idAdd || id == this.data.idMinus || id == this.data.idDivide || id == this.data.idMultiplication) {
            this.data.onceNum = false;
          if (this.data.lastIsOperator == true || this.data.screenData == 0) {
            return;
          }
        }
        //==================得到全局变量中的参数值 默认为0
        var sd = this.data.screenData;
        var data;
        if (sd == 0) {
          data = id;//如果参数是0
        } else {
          data = sd + id;
        }
        //==================给screenData设置数据
        this.setData({ screenData: data });
        this.data.arr.push(id);
        //判断是否是加减乘除，不能连续添加
        if (id == this.data.idAdd || id == data.idMinus || id == this.data.idDivide || id == this.data.idMultiplication) {
          this.setData({ lastIsOperator: true });
        } else {
          this.setData({ lastIsOperator: false });
        }
      }
    }
  })