const app = getApp()
Page({
    data: {
        userInfo: {},
        btnFlag: 0,
        //nav标志
        navFlag: 0,
        loanArray: ['商业贷款', '公积金贷款', '组合贷款'],
        loanIndex: 0,
        repaymentTypeArray: ['等额本息', '等额本金'],
        repaymentType: 0,
        lprRate: 3.95,
        bpRate: 0,
        totalRate: 3.95,
        accumulationFundRate: 2.85,
        //商贷金额
        businessLoan: "",
        //公积金贷金额
        accumulationLoan: "",
        //年限
        year: 5,
        result: {
            payment: "0.00",
            decline: "0.00",
            interest: "0.00",
            total: "0.00",
        },
        detail: {
            url: "",
            text: "还款详情",
            style: ""
        }
    },
    //事件处理函数
    bindViewRate: function () {
        wx.navigateTo({
            url: '../house/rate'
        })
    },
    onLoad: function () {
      
    },
    submit: function (e) {
        var that = this;
        var result = {};
        var detail = {};
        //等额本息
        if (that.data.navFlag == 0) {
            //商贷
            if (that.data.loanIndex == 0) {
                result = that.getResult(that.data.year, that.data.totalRate, that.data.businessLoan);
            }
            //公积金贷
            else if (that.data.loanIndex == 1) {
                result = that.getResult(that.data.year, that.data.accumulationFundRate, that.data.accumulationLoan);
            }
            //组合贷
            else {
                debugger;
                var result1, result2;
                result1 = result2 = {
                    payment: 0,
                    total: 0,
                    interest: 0
                }
                if (that.data.businessLoan != "") {
                    result1 = that.getResult(that.data.year, that.data.totalRate, that.data.businessLoan);
                }
                if (that.data.accumulationLoan != "") {
                    result2 = that.getResult(that.data.year, that.data.accumulationFundRate, that.data.accumulationLoan);
                }
                result = {
                    payment: result1.payment + result2.payment,
                    total: result1.total + result2.total,
                    interest: result1.interest + result2.interest
                }
            }

            var key1 = "result.payment";
            var key2 = "result.interest";
            var key3 = "result.total";
            that.setData({
                [key1]: result.payment.toFixed(2),
                [key2]: result.interest.toFixed(2),
                [key3]: result.total.toFixed(2),
            });
        }
        //等额本金
        else {
            //商贷、公积金贷
            if (that.data.loanIndex == 0) {
                result = that.getResult1(that.data.year, that.data.totalRate, that.data.businessLoan);
            }
            //公积金贷
            else if (that.data.loanIndex == 1) {
                result = that.getResult1(that.data.year, that.data.accumulationFundRate, that.data.accumulationLoan);
            }
            //组合贷
            else {
                var result1, result2;
                result1 = result2 = {
                    payment: 0,
                    total: 0,
                    interest: 0
                }
                if (that.data.businessLoan != "") {
                    result1 = that.getResult1(that.data.year, that.data.totalRate, that.data.businessLoan);
                }
                if (that.data.accumulationLoan != "") {
                    result2 = that.getResult1(that.data.year, that.data.accumulationFundRate, that.data.accumulationLoan);
                }
                result = {
                    payment: result1.payment + result2.payment,
                    decline: result1.decline + result2.decline,
                    total: result1.total + result2.total,
                    interest: result1.interest + result2.interest
                }
            }

            var key1 = "result.payment";
            var key2 = "result.decline";
            var key3 = "result.interest";
            var key4 = "result.total";
            that.setData({
                [key1]: result.payment.toFixed(2),
                [key2]: result.decline.toFixed(2),
                [key3]: result.interest.toFixed(2),
                [key4]: result.total.toFixed(2),
            })
        }

        //商贷
        if (that.data.loanIndex == 0) {
            detail = {
                text: "商贷还款详情",
                url: "../house/house-detail?flag=" + that.data.navFlag +
                    "&year=" + that.data.year +
                    "&businessLoan=" + that.data.businessLoan +
                    "&commercial=" + that.data.totalRate,
                style: "active"
            }
        }
        //公积金贷
        else if (that.data.loanIndex == 1) {
            detail = {
                text: "公积金贷还款详情",
                url: "../house/house-detail?flag=" + that.data.navFlag +
                    "&year=" + that.data.year +
                    "&accumulationLoan=" + that.data.accumulationLoan +
                    "&accumulationFundRate=" + that.data.accumulationFundRate,
                style: "active"
            }
        }
        //组合贷
        else {
            detail = {
                text: "组合贷款还款详情",
                url: "../house/house-detail?flag=" + that.data.navFlag +
                    "&year=" + that.data.year +
                    "&businessLoan=" + that.data.businessLoan +
                    "&commercial=" + that.data.totalRate +
                    "&accumulationLoan=" + that.data.accumulationLoan +
                    "&accumulationFundRate=" + that.data.accumulationFundRate,
                style: "active"
            }
        }
        that.setData({
            detail: detail,
            btnFlag: 1
        })
    },
    btnUrl: function (e) {
        var that = this;
        var url = that.data.detail.url;
        wx.navigateTo({
            url: url
        })
    },
    //nav切换
    repaymentTypeChange: function (e) {
        var that = this;
        that.setData({
            btnFlag: 0,
            navFlag: e.detail.value,
            repaymentType: e.detail.value,
            loanIndex: that.data.loanIndex,
            year: 5,
            lprRate: 3.95,
            bpRate: 0,
            totalRate: 3.95,
            accumulationFundRate: 2.85,
            businessLoan: "",
            accumulationLoan: "",
            result: {
                payment: "0.00",
                decline: "0.00",
                interest: "0.00",
                total: "0.00",
            },
            detail: {
                url: "",
                text: "还款详情",
                style: ""
            }
        })
    },
    bindLoanChange: function (e) {
        var that = this;
        var value = e.detail.value;
        if (value == 0) { //商贷
            that.setData({
                lprRate: 3.95,
                bpRate: 0,
                totalRate: 3.95,
            });
        } else if (value == 1) { //公积金贷
            that.setData({
                accumulationFundRate: 2.85,
            });
        } else { //组合贷款
            that.setData({
                loan: {
                    lprRate: 3.95,
                    bpRate: 0,
                    totalRate: 3.95,
                    accumulationFundRate: 2.85,
                },
            });
        }
        that.setData({
            btnFlag: 0,
            loanIndex: value,
            year: 5,
            lprRate: 3.95,
            bpRate: 0,
            totalRate: 3.95,
            accumulationFundRate: 2.85,
            businessLoan: "",
            accumulationLoan: "",
            result: {
                payment: "0.00",
                decline: "0.00",
                interest: "0.00",
                total: "0.00",
            },
            detail: {
                url: "",
                text: "还款详情",
                style: ""
            }
        });
    },
    lprRateInput: function (e) {
        var that = this;
        var lprRate = e.detail.value;
        if (/^\d+(\.\d+)?$/.test(lprRate)) {
            if (that.data.bpRate != '0') {
                that.setData({
                    totalRate: (parseFloat(lprRate) + parseFloat(that.data.bpRate / 100)).toFixed(2)
                })
            } else {
                that.setData({
                    totalRate: parseFloat(lprRate).toFixed(2)
                })
            }
        }
        that.setData({
            btnFlag: 0,
            lprRate: lprRate
        })
    },
    bpRateInput: function (e) {
        var that = this;
        var lprRate = parseFloat(that.data.lprRate);
        var bpRate = e.detail.value;
        if (/^\d+(\.\d+)?$/.test(lprRate)) {
            if (e.detail.value != '0') {
                that.setData({
                    totalRate: (lprRate + (parseFloat(bpRate) / 100)).toFixed(2)
                })
            } else {
                that.setData({
                    totalRate: lprRate
                })
            }
        }
        that.setData({
            btnFlag: 0,
            bpRate: bpRate
        })
    },
    providentFundChange: function (e) {
        var that = this;
        that.setData({
            btnFlag: 0,
            accumulationFundRate: e.detail.value
        })
    },
    commercialInput: function (e) {
        var that = this;
        that.setData({
            btnFlag: 0,
            businessLoan: e.detail.value,
            detail: {
                url: "",
                text: "还款详情",
                style: ""
            }
        })
    },
    providentFundInput: function (e) {
        var that = this;
        that.setData({
            btnFlag: 0,
            accumulationLoan: e.detail.value,
            detail: {
                url: "",
                text: "还款详情",
                style: ""
            }
        })
    },
    yearInput: function (e) {
        var that = this;
        var loanIndex = that.data.loanIndex,
            year = e.detail.value;
        var lprRate = 0;
        var accumulationFundRate = 0;
        if(year > 30){
            year = 30;
        }
        if (loanIndex == 0) {
            lprRate = year >= 5 ? 3.95 : 3.45;
            that.setData({
                year: year,
                lprRate: lprRate,
                detail: {
                    url: "",
                    text: "还款详情",
                    style: ""
                }
            })
        } else if (loanIndex == 1) {
            accumulationFundRate = year >= 5 ? 2.85 : 2.35;
            that.setData({
                year: year,
                accumulationFundRate: accumulationFundRate,
                detail: {
                    url: "",
                    text: "还款详情",
                    style: ""
                }
            })
        } else {
            lprRate = year >= 5 ? 3.95 : 3.45;
            accumulationFundRate = year >= 5 ? 2.85 : 2.35;
            that.setData({
                year: year,
                lprRate: lprRate,
                accumulationFundRate: accumulationFundRate,
                detail: {
                    url: "",
                    text: "还款详情",
                    style: ""
                }
            })
        }
        that.setData({
            btnFlag: 0,
        })
    },
    //等额本息公式
    getResult: function (year, rate, money) {
        //还款月数
        var months = year * 12;
        //月利率
        var monthRate = rate / 100 / 12;
        //月供
        var payment = money * 10000 * monthRate * Math.pow(1 + monthRate, months) / (Math.pow(1 + monthRate, months) - 1);
        var total = payment * months;
        //总利息
        var interest = total - money * 10000;
        return {
            payment: payment,
            total: total,
            interest: interest
        }
    },
    //等额本金公式
    getResult1: function (year, rate, money) {
        //贷款本金
        var p = money * 10000;
        //还款月数
        var months = year * 12;
        //月利率
        var monthRate = rate / 100 / 12;
        //月供
        var payment = (p / months) + p * monthRate;
        //月供递减
        var decline = p / months * monthRate;
        //总利息
        var interest = ((p / months + p * monthRate) + p / months * (1 + monthRate)) / 2 * months - p;
        //还款总额
        var total = interest + p;
        return {
            payment: payment,
            decline: decline,
            total: total,
            interest: interest
        }
    },
    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return {
            title: '车房贷款计算器',
            path: '/index/index',
            imageUrl: '../../images/logo.jpg',
            success: function (res) {
                // 转发成功
            },
            fail: function (res) {
                // 转发失败
            }
        }
    }
})