Page({
    /**
     * 页面的初始数据
     */
    data: {
      option: {
        flag: 0,
        year: 0,
        businessLoan: 0,
        accumulationLoan: 0,
        commercial: 0,
        accumulationFundRate: 0
      },
      table: [],
      //还款总额
      total: 0,
      //利息总额
      totalInterest: 0,
      //贷款总额
      totalPrincipal: 0,
      //贷款月数
      totalMonth: 0,
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      var that = this;
      that.setData({
        option: options
      });
      that.loadData();
    },
    loadData: function () {
      var that = this;
      var flag = that.data.option.flag;
      var year = that.data.option.year;
      var businessLoan = parseInt(that.data.option.businessLoan || 0);
      var accumulationLoan = parseInt(that.data.option.accumulationLoan || 0);
      var commercial = that.data.option.commercial || 0;
      var accumulationFundRate = that.data.option.accumulationFundRate || 0;
      var result = {};
      if (flag == 0) {              //等额本息
        var result1, result2;
        result1 = result2 = {
          payment: 0,
          total: 0,
          interest: 0
        }
        if (businessLoan != 0) {
          result1 = that.getPrincipalInterest(year, commercial, businessLoan);
        }
        if (accumulationLoan != 0) {
          result2 = that.getPrincipalInterest(year, accumulationFundRate, accumulationLoan);
        }
        result = {
          payment: result1.payment + result2.payment,
          total: result1.total + result2.total,
          interest: result1.interest + result2.interest
        }
  
        var table = [];
        var t1 = businessLoan * 10000;
        var t2 = accumulationLoan * 10000;
        var t = result.total;
        var tp = t1 + t2;
        var monthCommercial = commercial / 100 / 12;
        var monthAccumulationFundRate = accumulationFundRate / 100 / 12;
  
        for (var i = 0; i < year; i++) {
          var arr = []
          for (var j = 0; j < 12; j++) {
            var interest = t1 * monthCommercial + t2 * monthAccumulationFundRate;
            var principal = result1.payment + result2.payment - interest;
            var item = {};
            var remainder = tp - principal;
            //应还利息
            item.interest = interest.toFixed(2);
            //应还本金
            item.principal = principal.toFixed(2);
            // 月供总额
            item.totalAmount = Math.abs(parseFloat(item.interest) + parseFloat(item.principal)).toFixed(2);
            //剩余金额
            item.remainder = Math.abs(remainder).toFixed(2);
            t1 = t1 - (result1.payment - t1 * monthCommercial);
            t2 = t2 - (result2.payment - t2 * monthAccumulationFundRate);
            t = remainder;
            tp = remainder;
            arr.push(item);
          }
          table.push(arr);
        }
  
        that.setData({
          table: table
        });
  
      } else if (flag == 1) {       //等额本金
        var result1, result2;
        result1 = result2 = {
          payment: 0,
          total: 0,
          interest: 0
        }
        if (businessLoan != 0) {
          result1 = that.getAverageCapital(year, commercial, businessLoan);
        }
        if (accumulationLoan != 0) {
          result2 = that.getAverageCapital(year, accumulationFundRate, accumulationLoan);
        }
        result = {
          payment: result1.payment + result2.payment,
          decline: result1.decline + result2.decline,
          total: result1.total + result2.total,
          interest: result1.interest + result2.interest
        }
  
        var table = [];
        var t1 = businessLoan * 10000;
        var t2 = accumulationLoan * 10000;
        var t = result.total;
        var tp = t1 + t2;
        var principal1 = 0;
        var principal2 = 0;
        var monthCommercial = commercial / 100 / 12;
        var monthAccumulationFundRate = accumulationFundRate / 100 / 12;
        for (var i = 0; i < year; i++) {
          var arr = []
          for (var j = 0; j < 12; j++) {
            var principal = businessLoan * 10000 / year / 12 + accumulationLoan * 10000 / year / 12;
            var interest = t1 * monthCommercial + t2 * monthAccumulationFundRate;
            var remainder = tp - principal;
            var item = {};
            //应还利息
            item.interest = interest.toFixed(2);
            //应还本金
            item.principal = principal.toFixed(2);
            // 月供总额
            item.totalAmount = Math.abs(parseFloat(item.interest) + parseFloat(item.principal)).toFixed(2);
            //剩余金额
            item.remainder = Math.abs(remainder).toFixed(2);
            t1 = t1 - businessLoan * 10000 / year / 12;
            t2 = t2 - accumulationLoan * 10000 / year / 12;
            principal1 += businessLoan * 10000 / year / 12;
            principal2 += accumulationLoan * 10000 / year / 12;
            t = remainder;
            tp = remainder;
            arr.push(item);
          }
          table.push(arr);
        }
  
        that.setData({
          table: table
        });
  
      }
      that.setData({
        total: (result.total / 10000).toFixed(2),
        totalInterest: (result.total / 10000 - businessLoan - accumulationLoan).toFixed(2),
        totalPrincipal: businessLoan + accumulationLoan,
        totalMonth: year * 12
      });
    },
    //等额本息公式
    getPrincipalInterest: function (year, rate, money) {
      //还款月数
      var months = year * 12;
      //月利率
      var monthRate = rate / 100 / 12;
      //月供
      var payment = money * 10000 * monthRate * Math.pow(1 + monthRate, months) / (Math.pow(1 + monthRate, months) - 1);
      var total = payment * months;
      //总利息
      var interest = total - money * 10000;
      return {
        payment: payment,
        total: total,
        interest: interest,
      }
    },
    //等额本金公式
    getAverageCapital: function (year, rate, money) {
      //贷款本金
      var p = money * 10000;
      //还款月数
      var months = year * 12;
      //月利率
      var monthRate = rate / 100 / 12;
      //月供
      var payment = (p / months) + p * monthRate;
      //月供递减
      var decline = p / months * monthRate;
      //总利息
      var interest = ((p / months + p * monthRate) + p / months * (1 + monthRate)) / 2 * months - p;
      //还款总额
      var total = interest + p;
      return {
        payment: payment,
        decline: decline,
        total: total,
        interest: interest
      }
    },
  })